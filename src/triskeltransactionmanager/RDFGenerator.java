package triskeltransactionmanager;

import cubetriplestore.CubeTripleStore;
import cubetriplestore.CubeTripleStoreSerializer;
import java.util.Date;
import termstore.TermStore;
import termstore.TermStoreSerialize;
import termstore.avltree.AvlTermStore;
import termstore.avltree.AvlTermStoreSerialize;
import triple.Triple;
import triplestore.TripleStore;
import triplestore.TripleStoreSerializer;
import triskelbuilder.Mounter;
import triskelbuilder.SnapShotBuilder;

public class RDFGenerator {

    private static final int numberOfEvents = 500000;

    public static void main(String[] args) throws Exception {
        TransactionGenerator transactionGenerator = new TransactionGenerator();
        transactionGenerator.generate(numberOfEvents);
        TripleStore tripleStore = new CubeTripleStore();
        TermStore termStore = new AvlTermStore();
        
        Mounter mounter = new Mounter(tripleStore, termStore);
        Date date = new Date();
        Long time = date.getTime();
        mounter.mount();
        Date date1 = new Date();
        System.out.println((date1.getTime() - time));
        System.out.println(mounter.tripleStore.checkTriple(new Triple(0, 0, 0)));
        TripleStoreSerializer tripleStoreSerializer = new CubeTripleStoreSerializer(tripleStore);
        TermStoreSerialize termStoreSerialize = new AvlTermStoreSerialize((AvlTermStore) termStore);
        SnapShotBuilder snapShotBuilder = new SnapShotBuilder(tripleStoreSerializer, termStoreSerialize);
        snapShotBuilder.execute(Long.toString(date.getTime()));
    }
}
