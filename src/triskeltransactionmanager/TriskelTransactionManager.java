package triskeltransactionmanager;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import triskelbuilder.events.DataEvents;

public class TriskelTransactionManager {

    private static TriskelTransactionManager triskelTransactionManager = null;
    private long totalElements;

    private TriskelTransactionManager() {
        totalElements = 0;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public static synchronized TriskelTransactionManager getInstance() {
        return (triskelTransactionManager == null) ? new TriskelTransactionManager() : triskelTransactionManager;
    }

    public void add(DataEvents event, ObjectOutputStream objectOutputStream) {
        try {
            objectOutputStream.writeObject(event);
        } catch (IOException ex) {
            Logger.getLogger(TriskelTransactionManager.class.getName()).log(Level.SEVERE, null, ex);
            Runtime.getRuntime().exit(0);
        }
        totalElements++;
    }
}
