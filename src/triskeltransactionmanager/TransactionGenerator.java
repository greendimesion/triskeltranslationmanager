package triskeltransactionmanager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import triskelbuilder.events.DataEvents;

public class TransactionGenerator {

    private Integer limite = 1000;
    TriskelTransactionManager eventProcessor = null;
    private File fileSubjects;
    private File filePredicates;
    private File fileObjects;
    private Integer eventsgenerated = 0;
    private BufferedReader bufferedReader;
    private FileReader fileReader;

    public TransactionGenerator() {
        this.eventProcessor = TriskelTransactionManager.getInstance();

        fileSubjects = new File("Subjects.txt");
        filePredicates = new File("Predicates.txt");
        fileObjects = new File("Objects.txt");
    }

    public int eventsGenerated() {
        return eventsgenerated;
    }

    public void generate(int eventsNumber) throws Exception {
        File file = new File("Events.dat");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));
        DataEvents dataEvent;

        eventProcessor = TriskelTransactionManager.getInstance();

        try {
            eventProcessor = TriskelTransactionManager.getInstance();
            String subject = null, predicate = null, object = null;
            for (int i = 0; i < eventsNumber; i++) {
                subject = giveOnePartTripleData(fileSubjects);
                predicate = giveOnePartTripleData(filePredicates);
                object = giveOnePartTripleData(fileObjects);
                char action = getRandomAction();
                eventProcessor.add(new DataEvents(subject, predicate, object, action), objectOutputStream);
                eventsgenerated++;
            }

            eventProcessor.add(new DataEvents(subject, predicate, object, 'F'), objectOutputStream);
            objectOutputStream.close();

        } catch (Exception exception) {
            Logger.getLogger(TriskelTransactionManager.class.getName()).log(Level.SEVERE, null, exception);
            Runtime.getRuntime().exit(0);
        }

    }

    private int randomNumber() {
        return ((int) ((Math.random() * limite) + 1));
    }

    private void openFile(File nameFile) throws FileNotFoundException {
        fileReader = new FileReader(nameFile);
        bufferedReader = new BufferedReader(fileReader);
    }

    private String selectEventComponent(int objectToRead, File file) throws IOException {
        String lineOfFile = null;
        for (int j = 0; j < objectToRead; j++) {
            lineOfFile = bufferedReader.readLine();
            if (lineOfFile == null) {
                fileReader.close();
                openFile(file);
                lineOfFile = bufferedReader.readLine();
            }
        }
        return lineOfFile;
    }

    private char getRandomAction() {
        char[] actions = {'A', 'E'};
        return actions[(int) ((Math.random() + 1)) - 1];
    }

    private String giveOnePartTripleData(File file) throws FileNotFoundException, IOException {
        openFile(file);
        int objectToRead = randomNumber();
        String result = selectEventComponent(objectToRead, file);
        fileReader.close();
        return result;
    }
}
